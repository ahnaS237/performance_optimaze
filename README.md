# UI 성능 최적화 가이드
> 해당 프로젝트는 [프론트엔드 성능 최적화 가이드]를 중점으로 한 UI 성능 최적화 관련 예제입니다.

## 기능
프로젝트 내에서는 다음의 UI 최적화를 적용합니다
- **이미지 사이즈 최적화:** 화면에서 필요한 이미지 사이즈의 2배로 이미지를 받아오거나 변경
- **코드 분할:** 첫 화면에 미리 로딩하지 않아도 되는 컴포넌트들을 분할하여 로딩
- **빌드를 통한 파일 사이즈 경량화:** build를 실행하면서 압축되는 파일로 인해 소스의 경량화가 진행된다
- **Css 애니메이션 최적화 :** css 에서 리플로우, 리페인팅되는 요소를 GPU를 사용하는 style로 변경
- **컴포넌트 사전 로딩 :** 컴포넌트를 특정 시점에 사전 로드하여 유연한 UI 화면을 제공한다.
- **이미지 사전 로딩:** 특정 이미지를 사전에 로드하여 유연한 UI 화면을 제공한다.
- **이미지 지연 로딩 :** 사용하지 않는 이미지를 분리하여 추후 로딩시키도록 한다.
- **폰트 최적화:** 폰트를 최적화 한다.
- **캐시 최적화:** cache-control을 사용하여 기존에 사용한 데이터를 재사용할 수 있도록 한다.

## 사용 방법

1. 설치 단계
    ```bash
    npm install
    ```

2. 실행 단계 (client 사이드)
    ```bash
    npm run start
    ```

3. 실행 단계 (server 사이드) -> json-server를 DB 대신 사용
    ```bash
    node ./node_modules/json-server/lib/cli/bin.js --watch ./server/database.json -c ./server/config.json
    ```

## 예시

![프로젝트 예시](./images/image1.png)


<aside>
💡 ListPage에서는 /static/js/src_pages_ListPage_index_js.chunk.js 만 가져온다
</aside>


![프로젝트 예시](./images/image.png)


<aside>
💡 ViewPage에서는 /static/js/src_pages_ViewPage_index_js.chunk.js 만 가져온다
</aside>


## 기술 스택

- 언어: react, node.js, express.js, axios
- Runtime 환경: CSR, SSR
- 데이터베이스: json-server 라이브러리로 데이터베이스 대신 json을 사용 (/server/database.json)


## 기여 방법

1. Fork 프로젝트
2. 새로운 브랜치 생성 (`git checkout -b develop`)
3. 브랜치 푸시 (`git push -uf origin develop`)
4. Pull Request 생성

## 문제 해결

localhost 상태에서 ssl에 대한 처리가 추가적으로 필요.

- "--openssl-legacy-provider" 옵션은 React 애플리케이션을 개발 서버에서 실행할 때 OpenSSL의 레거시(이전 버전) 프로바이더를 사용하도록 하는 옵션

```bash
react-scripts --openssl-legacy-provider start
```
