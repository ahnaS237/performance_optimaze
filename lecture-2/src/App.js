import React, { Suspense, lazy, useEffect, useState } from "react";
import styled from "styled-components";
import Header from "./components/Header";
import InfoTable from "./components/InfoTable";
import SurveyChart from "./components/SurveyChart";
import Footer from "./components/Footer";
//import ImageModal from './components/ImageModal'
// 동적 component import --> 한번 import하면 이후부터 캐싱된 내용을 사용 (컴포넌트 로딩을 추가적으로 하지 않는다)
const ImageModal = lazy(() => import("./components/ImageModal"));

function App() {
  const [showModal, setShowModal] = useState(false);
  // 마우스 오버할때 컴토넌트를 import!
  //   const handleMouseEnter = () => {
  //   const component = import("./components/ImageModal");
  //   const img = new Image(); // 이미지 사전 로딩해두기 (disk cache된 이미지를 로드)
  //   img.src =
  //     "https://stillmed.olympic.org/media/Photos/2016/08/20/part-1/20-08-2016-Football-Men-01.jpg?interpolation=lanczos-none&resize=*:800";
  //   };

  // 마운트 완료 시점에 컴포넌트를 import!
  useEffect(() => {
    const component = import("./components/ImageModal"); // import 사전에 진행해 메모리에 둔다.
    const img = new Image(); // 이미지 사전 로딩해두기 (disk cache된 이미지를 로드)
    img.src =
      "https://stillmed.olympic.org/media/Photos/2016/08/20/part-1/20-08-2016-Football-Men-01.jpg?interpolation=lanczos-none&resize=*:800";
  }, []);

  return (
    <div className="App">
      <Header />
      <InfoTable />
      <ButtonModal
        onClick={() => {
          setShowModal(true);
        }}
        //onMouseEnter={handleMouseEnter}
      >
        올림픽 사진 보기
      </ButtonModal>
      <SurveyChart />
      <Footer />
      <Suspense fallback={null}>
        {showModal ? (
          <ImageModal
            closeModal={() => {
              setShowModal(false);
            }}
          />
        ) : null}
      </Suspense>
    </div>
  );
}

const ButtonModal = styled.button`
  border-radius: 30px;
  border: 1px solid #999;
  padding: 12px 30px;
  background: none;
  font-size: 1.1em;
  color: #555;
  outline: none;
  cursor: pointer;
`;

export default App;
