import React, { useEffect, useState, useCallback } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import ReactMarkdown from "react-markdown";

import styles from "./index.module.css";
import BasicTemplates from "../../templates/BasicTemplates";
import CodeBlock from "../../components/markdowns/CodeBlock";

function ViewPage(props) {
  const { id } = useParams();
  const [article, setArticle] = useState(false);

  // 게시글 가져오기
  const getArticle = useCallback(
    (id) => {
      axios.get("http://localhost:3001/articles/" + id).then((success) => {
        setArticle(success.data);
      });
    },
    [id]
  );

  /* 파라미터 참고: https://unsplash.com/documentation#supported-parameters */
  function getParametersForUnsplash({ width, height, quality, format }) {
    return `?w=${width}&h=${height}&q=${quality}&fm=${format}&fit=crop`;
  }

  useEffect(() => {
    getArticle(id);
  }, [getArticle]);

  return article ? (
    <BasicTemplates>
      <div className={`${styles.ViewPage}`}>
        <h1 className={`${styles.ViewPage__title}`}>{article.title}</h1>
        {/* <img className={"ViewPage__image"} src={article.image} alt="thumnail" /> */}
        <img
          className={`${styles.ViewPage__image}`}
          src={
            article.image +
            getParametersForUnsplash({
              width: 1400,
              height: 1060,
              quality: 80,
              format: "jpg",
            })
          }
          alt="thumbnail"
        />
        <div className={`${styles.ViewPage__content}`}>
          <ReactMarkdown
            children={article.content}
            components={CodeBlock}
          />
        </div>
      </div>
    </BasicTemplates>
  ) : (
    <h1>loading...</h1>
  );
}

export default ViewPage;
